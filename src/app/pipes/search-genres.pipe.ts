import { Pipe, PipeTransform } from '@angular/core';
import { GenerateObservable } from 'rxjs/observable/GenerateObservable';
import { MovieCardComponent } from '../components/movie-card/movie-card.component'

@Pipe({
  name: 'searchGenres'
})
export class SearchGenresPipe implements PipeTransform {

  transform(value: number | string): string {
    if (value === 28) {
      return value = "Action";
    }
    else if (value === 12) {
      return value = "Aventure";
    }
    else if (value === 16) {
      return value = "Animation";
    }
    else if (value === 35) {
      return value = "Comédie";
    }
    else if (value === 80) {
      return value = "Crime";
    }
    else if (value === 99) {
      return value = "Documentaire";
    }
    else if (value === 18) {
      return value = "Drame";
    }
    else if (value === 10751) {
      return value = "Famille";
    }
    else if (value === 14) {
      return value = "Fantastique";
    }
    else if (value === 10769) {
      return value = "Film étranger";
    }
    else if (value === 36) {
      return value = "Historique";
    }
    else if (value === 27) {
      return value = "Horreur";
    }
    else if (value === 10402) {
      return value = "Musical";
    }
    else if (value === 9648) {
      return value = "Mystère";
    }
    else if (value === 10749) {
      return value = "Romantique";
    }
    else if (value === 878) {
      return value = "Science Fiction";
    }
    else if (value === 10770) {
      return value = "Téléfilm";
    }
    else if (value === 53) {
      return value = "Thriller";
    }
    else if (value === 10752) {
      return value = "Guerre";
    }
    else if (value === 37) {
      return value = "Western";
    }
    else {
      return value = "Genre non défini" 
    }
  }

}
