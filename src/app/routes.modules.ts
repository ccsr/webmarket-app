import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './main components/home/home.component';
import { MoviePageComponent } from './main components/movie-page/movie-page.component';
import { AppComponent } from './app.component';
import { ResultsMainComponent } from './main components/results-main/results-main.component';
import { RentPageComponent } from './main components/rent-page/rent-page.component';
import { MainCastingDetailsComponent } from './main components/main-casting-details/main-casting-details.component';
import { MainSignupComponent } from './main components/main-signup/main-signup.component';
import { MainSigninComponent } from './main components/main-signin/main-signin.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'movie/:id', component: MoviePageComponent },
    { path: 'results/:query', component: ResultsMainComponent },
    { path: 'rent/:id', component: RentPageComponent },
    { path: 'people/:id', component: MainCastingDetailsComponent },
    { path: 'signup', component: MainSignupComponent },
    { path: 'signin', component: MainSigninComponent }
    
]


@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  })
  
  export class RoutesModule {}