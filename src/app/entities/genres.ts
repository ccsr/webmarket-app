export interface Genres {
    id: number,
    en: string,
    fr: string
}