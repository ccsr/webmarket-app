export interface Cast {
    cast_id: number,
    character: string,
    credit_id: number,
    gender: number | null,
    id: number,
    name: string,
    order: number,
    profile_path: string
}