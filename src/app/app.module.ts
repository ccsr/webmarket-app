import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {JsonpModule, Jsonp, Response} from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { AppComponent } from './app.component';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MovieCardComponent } from './components/movie-card/movie-card.component';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CarouselLastMoviesComponent } from './components/carousel-last-movies/carousel-last-movies.component';
import { SectionAlauneComponent } from './components/section-alaune/section-alaune.component';
import { SearchFieldComponent } from './components/search-field/search-field.component';
import { RandomMoviesComponent } from './components/random-movies/random-movies.component';
import { ResultsComponent } from './components/results/results.component'
import { DataService } from './services/data.service';
import { SearchGenresPipe } from './pipes/search-genres.pipe';
import { HomeComponent } from './main components/home/home.component';
import { MoviePageComponent } from './main components/movie-page/movie-page.component';
import { CastingComponent } from './components/casting/casting.component';
import { RelatedMoviesComponent } from './components/related-movies/related-movies.component';
import { RentPageComponent } from './main components/rent-page/rent-page.component';
import { ResultsMainComponent } from './main components/results-main/results-main.component';
import { CastingDetailsComponent } from './components/casting-details/casting-details.component';

import {RoutesModule} from './routes.modules';
import { MainCastingDetailsComponent } from './main components/main-casting-details/main-casting-details.component';
import { CastingsMoviesComponent } from './components/castings-movies/castings-movies.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { MainSignupComponent } from './main components/main-signup/main-signup.component';
import { MainSigninComponent } from './main components/main-signin/main-signin.component';
import { TheatreMoviesComponent } from './components/theatre-movies/theatre-movies.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieDetailsComponent,
    MovieCardComponent,
    MoviesListComponent,
    HeaderComponent,
    FooterComponent,
    CarouselLastMoviesComponent,
    SectionAlauneComponent,
    SearchFieldComponent,
    RandomMoviesComponent,
    ResultsComponent,
    SearchGenresPipe,
    HomeComponent,
    MoviePageComponent,
    ResultsMainComponent,
    RentPageComponent,
    RelatedMoviesComponent,
    CastingComponent,
    CastingDetailsComponent,
    MainCastingDetailsComponent,
    CastingsMoviesComponent,
    SigninComponent,
    SignupComponent,
    MainSignupComponent,
    MainSigninComponent,
    TheatreMoviesComponent
  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    HttpClientModule,
    FormsModule,
    JsonpModule,
    RouterModule,
    RoutesModule,
    ReactiveFormsModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
registerLocaleData(localeFr, 'fr');