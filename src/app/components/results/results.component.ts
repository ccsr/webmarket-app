import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss'],
  providers: [DataService]
})
export class ResultsComponent implements OnInit {

  constructor(private dataService: DataService) { }

  movies: any
  query:string

  ngOnInit() {

  }

  onSearch() {
    this.dataService.searchMovie(this.query).subscribe(
      data => { this.movies = data.results }, 
      err => { this.movies = err.message },
      () => console.log('done loading movie(s)')
    );
  }
}
