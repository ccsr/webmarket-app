import { Component, OnInit } from "@angular/core";
import { DataService } from "../../services/data.service";
import { ActivatedRoute } from "@angular/router";

import { Recommandations } from "../../entities/recommandations"

@Component({
  selector: "app-related-movies",
  templateUrl: "./related-movies.component.html",
  styleUrls: ["./related-movies.component.scss"]
})
export class RelatedMoviesComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => this.getMovieByID(params["id"]));
    this.route.params.subscribe(params => this.getRelatedMovies(params["id"]));
  }

  relatedMovies: object;
  movie: object;

  getRelatedMovies(id: number) {
    this.relatedMovies = this.dataService.getRelatedMovies(id);
  }

  getMovieByID(id: number) {
    this.movie = this.dataService.getDetails(id);
  }
}
