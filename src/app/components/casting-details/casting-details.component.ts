import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-casting-details',
  templateUrl: './casting-details.component.html',
  styleUrls: ['./casting-details.component.scss']
})
export class CastingDetailsComponent implements OnInit {

    constructor(private dataService : DataService, private route : ActivatedRoute) {
      this.route.params.subscribe(params => this.getCastingDetails(params["id"]));
      this.route.params.subscribe(params => this.getMovieCredits(params["id"]));
      this.route.params.subscribe(params => this.getCastExternalId(params["id"]));
      this.route.params.subscribe(params => this.getBestMovies(params["id"]));
     }

  ngOnInit() {
  }

  profileURL: string = "https://image.tmdb.org/t/p/w185"
  casting: object
  moviesByCast: object
  cast_external_id: object
  bestMovies: object

  getCastingDetails(id) {
    this.casting = this.dataService.getCastingDetails(id);
  }  

  getMovieCredits(id) {
    this.moviesByCast = this.dataService.getMovieByCast(id);
  }

  getCastExternalId(id:number) {
    this.cast_external_id = this.dataService.getCastExternalId(id);
  }

  getBestMovies(id :number) {
    this.bestMovies = this.dataService.getBestMovies(id)
  }
}
