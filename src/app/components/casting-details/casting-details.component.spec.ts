import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CastingDetailsComponent } from './casting-details.component';

describe('CastingDetailsComponent', () => {
  let component: CastingDetailsComponent;
  let fixture: ComponentFixture<CastingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CastingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CastingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
