import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-castings-movies',
  templateUrl: './castings-movies.component.html',
  styleUrls: ['./castings-movies.component.scss']
})
export class CastingsMoviesComponent implements OnInit {

  constructor(private dataService : DataService, private route : ActivatedRoute) {
    this.route.params.subscribe(params => this.getMovieCredits(params["id"]));
    this.route.params.subscribe(params => this.getCastingDetails(params["id"]));
   }

  ngOnInit() {
  }

  posterURL: string = "https://image.tmdb.org/t/p/w154"
  moviesByCast: object
  casting: object

  getCastingDetails(id) {
    this.casting = this.dataService.getCastingDetails(id);
  }

  getMovieCredits(id) {
    this.moviesByCast = this.dataService.getMovieByCast(id);
  }

}
