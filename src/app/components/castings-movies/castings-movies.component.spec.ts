import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CastingsMoviesComponent } from './castings-movies.component';

describe('CastingsMoviesComponent', () => {
  let component: CastingsMoviesComponent;
  let fixture: ComponentFixture<CastingsMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CastingsMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CastingsMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
