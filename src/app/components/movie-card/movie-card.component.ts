import { Component, OnInit, Input } from '@angular/core';
import { ResultsSearch } from '../../entities/search-movies';
import { generate } from 'rxjs/observable/generate';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  
  @Input() title: string
  @Input() language: string
  @Input() year: string
  @Input() genre1: any
  @Input() genre2: any
  @Input() backdrop: string
  @Input() id: number

  image300URL: string = "https://image.tmdb.org/t/p/w300";
  
  constructor() { 
  }

  ngOnInit() {
  }

}
