import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-casting',
  templateUrl: './casting.component.html',
  styleUrls: ['./casting.component.scss']
})
export class CastingComponent implements OnInit {

    constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => this.getCasting(params["id"]));
  }

  casting:object
  profilePic: string = "https://image.tmdb.org/t/p/w185";  

  getCasting(id) {
    this.casting = this.dataService.getCredits(id);
  }

}
