import { Component, OnInit } from "@angular/core";
import { DataService } from "../../services/data.service";

@Component({
  selector: "app-theatre-movies",
  templateUrl: "./theatre-movies.component.html",
  styleUrls: ["./theatre-movies.component.scss"]
})
export class TheatreMoviesComponent implements OnInit {
  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.getTheatreMovies();
  }

  year: Date = new Date();
  currentYear = this.year.getFullYear().toString()

  getTheatreMovies() {
    let theaterMovies = this.dataService.getTheatreMovies()
  }
}
