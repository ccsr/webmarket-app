import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-section-alaune',
  templateUrl: './section-alaune.component.html',
  styleUrls: ['./section-alaune.component.scss'],
  providers: [DataService]
})
export class SectionAlauneComponent implements OnInit {

  constructor(private dataService: DataService) {
   }

  ngOnInit() {  
    this.getLastMovies();
  }
  
  year: Date = new Date();
  currentYear = this.year.getFullYear().toString()
  lastMovies: object

  getLastMovies() {
    let lastMovies = this.dataService.getLastMoviesByDate().subscribe(
      data => { this.lastMovies = data.results }
    )
  }
}