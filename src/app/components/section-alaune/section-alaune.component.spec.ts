import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionAlauneComponent } from './section-alaune.component';

describe('SectionAlauneComponent', () => {
  let component: SectionAlauneComponent;
  let fixture: ComponentFixture<SectionAlauneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionAlauneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionAlauneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
