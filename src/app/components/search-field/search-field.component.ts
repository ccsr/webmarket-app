import { Component, OnInit } from '@angular/core';
import { Genres } from '../../entities/genres';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.scss'],
  providers: [DataService]
})
export class SearchFieldComponent implements OnInit {

  allYears: string[] = ["1950", "1951", "1952", "1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018"];
  years: string[] = this.allYears.reverse();

  genres: Genres[] = [{ id: 28, en: "Action", fr: "Action" }, { id: 12, en: "Adventure", fr: "Aventure" }, { id: 16, en: "Animation", fr: "Animation" }, { id: 35, en: "Comedy", fr: "Comédie" }, { id: 80, en: "Crime", fr: "Crime" }, { id: 99, en: "Documentary", fr: "Documentaire" }, { id: 18, en: "Drama", fr: "Drame" }, { id: 10751, en: "Family", fr: "Famille" }, { id: 14, en: "Fantasy", fr: "Fantastique" }, { id: 36, en: "History", fr: "Historique" }, { id: 27, en: "Horror", fr: "Horreur" }, { id: 10402, en: "Music", fr: "Musical" }, { id: 10749, en: "Romance", fr: "Romantique" }, { id: 878, en: "Science Fiction", fr: "Science Fiction" }, { id: 10770, en: "TV Movie", fr: "Film TV" }, { id: 53, en: "Thriller", fr: "Thriller" }, { id: 10752, en: "War", fr: "Guerre" }, { id: 37, en: "Western", fr: "Western" }];

  constructor(private dataService: DataService) { }

  res: string
  movies: any
  query: string

  // getByGenre(query) {
  //   this.dataService.getMoviesByGenre(query).subscribe(
  //     data => { this.res = data.results[0].name }
  //   )
  // }

  onSearch() {
    this.dataService.searchMovie(this.query).subscribe(
      data => { this.movies = data.results },
      err => { this.movies = err.message },
      () => console.log('done loading movie(s)')
    );
  }


  ngOnInit() {
  }

}
