import { Component, OnInit } from "@angular/core";
import { DataService } from "../../services/data.service";
import { ActivatedRoute } from "@angular/router";
import { Cast } from "../../entities/cast"
import { Observable, ObservableInput } from "rxjs";

@Component({
  selector: "app-movie-details",
  templateUrl: "./movie-details.component.html",
  styleUrls: ["./movie-details.component.scss"]
})
export class MovieDetailsComponent implements OnInit {
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute
  ) { }

  movie: Observable<string>;
  movies: any;
  relatedMovies: any;
  posterURL: string = "https://image.tmdb.org/t/p/w342";
  backdropURL: string = "https://image.tmdb.org/t/p/original";
  external_id: any;
  id: number;
  credits: any;

  ngOnInit() {
    this.route.params.subscribe(params => this.getMovieByID(params["id"]));
    this.route.params.subscribe(params => this.getExternalIds(params["id"]));
  }

  getMovieByID(id: number) {
    this.movie = this.dataService.getDetails(id);
  }

  getExternalIds(id: number) {
    this.external_id = this.dataService.getExternalId(id)
  }

  
}
