import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-carousel-last-movies',
  templateUrl: './carousel-last-movies.component.html',
  styleUrls: ['./carousel-last-movies.component.scss']
})
export class CarouselLastMoviesComponent implements OnInit {

  constructor(private dataService : DataService) { }

  movies:any
  imageOriginalURL: string = "https://image.tmdb.org/t/p/original";
  image1280URL: string = "https://image.tmdb.org/t/p/w1280"

  ngOnInit() {
    this.dataService.getPopularMovies().subscribe(
      data => { this.movies = data.results }
    )
  }

}
