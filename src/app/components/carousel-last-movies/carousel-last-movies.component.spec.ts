import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselLastMoviesComponent } from './carousel-last-movies.component';

describe('CarouselLastMoviesComponent', () => {
  let component: CarouselLastMoviesComponent;
  let fixture: ComponentFixture<CarouselLastMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselLastMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselLastMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
