import { Component, OnInit } from '@angular/core';
import { DataService } from "../../services/data.service";
import { SearchMovies, ResultsSearch } from '../../entities/search-movies';
import * as firebase from 'firebase';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [DataService]
})
export class HeaderComponent implements OnInit {

  query: string
  movies: ResultsSearch

  isAuth: boolean

  constructor(private dataService: DataService, private authService: AuthService) { }

  ngOnInit() {
    firebase.auth().onAuthStateChanged(
      (user) => { 
        if(user) {
          this.isAuth = true;
        }else {
          this.isAuth = false;
        }
      }
    )
  }

  onSignOut() {
    this.authService.signOutUser();
  }

  onSearch() {
    this.dataService.searchMovie(this.query).subscribe(
      data => {
        this.movies = data.results;
        // let titles = data.results.map(element => element.title);
        // let overviews = data.results.map(element => element.overview);
        // let popularity = data.results.map(element => element.popularity);
        // let language = data.results.map(element => element.original_language);
        // let picture = data.results.map(element => element.poster_path)
      },

      //                  <!> ALTERNATIVE VERSION <!>
      // let titles: string[] = [];
      // let overviews: string[] = [];
      // data.results.forEach(element => {
      //   titles.push(element.title);
      //   overviews.push(element.overview)
      // }) ;
      // console.log(data)
      // this.movie = data },

      err => { this.movies = err.message },
      () => console.log('done loading movie(s)')
    );
  }
}
