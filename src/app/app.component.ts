import { Component } from '@angular/core';
import * as firebase from 'firebase'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
      // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDndR5W9MNaXQNpiS9l7a6ZZMeL4kg4qWI",
    authDomain: "ccauser-cineclick.firebaseapp.com",
    databaseURL: "https://ccauser-cineclick.firebaseio.com",
    projectId: "ccauser-cineclick",
    storageBucket: "",
    messagingSenderId: "1078658564475"
  };
  firebase.initializeApp(config);

  }
}
