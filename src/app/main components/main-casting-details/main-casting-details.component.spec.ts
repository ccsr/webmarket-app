import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainCastingDetailsComponent } from './main-casting-details.component';

describe('MainCastingDetailsComponent', () => {
  let component: MainCastingDetailsComponent;
  let fixture: ComponentFixture<MainCastingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainCastingDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCastingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
