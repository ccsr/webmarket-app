import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import "rxjs/add/operator/map";
import "rxjs/Rx";
import { Http } from "@angular/http";

@Injectable()
export class DataService {
  private baseUrl: string = "https://api.themoviedb.org/3";
  private imageBaseUrl: string = "https://image.tmdb.org/t/p/";

  private lang: string = "fr-FR";
  private API_KEY: string = "b875d98d6eb8188277dedea4f187fe79";

  private searchUri: string = "/search/movie";
  private getPopular: string = "/discover/movie?sort_by=popularity.desc";
  private movieDetails: string = "/movie/";

  private currentYear: Date = new Date('yyyy');

  constructor(private http: HttpClient) {}

  searchMovie(query: string): Observable<any> {
    return this.http.get(`${this.baseUrl}${this.searchUri}`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
        .set("query", query)
    });
  }

  getPopularMovies(): Observable<any> {
    return this.http.get(`${this.baseUrl}${this.getPopular}`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getLastMoviesByDate(): Observable<any> {
    return this.http.get(`${this.baseUrl}/discover/movie`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
        .set('sort_by', 'vote_average.desc')
        .set('primary_release_year', '2018')
    });
  }

  getDetails(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}${this.movieDetails}${id}`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getRelatedMovies(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/movie/${id}/recommendations`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
        .set('sort_by', 'vote_average.desc')
    });
  }

  getExternalId(id: number) {
    return this.http.get(`${this.baseUrl}/movie/${id}/external_ids`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getCredits(id: number) {
    return this.http.get(`${this.baseUrl}/movie/${id}/credits`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getCastingDetails(id: number) {
    return this.http.get(`${this.baseUrl}/person/${id}`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getMovieByCast(id: number) {
    return this.http.get(`${this.baseUrl}/person/${id}/movie_credits`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getCastExternalId(id: number) {
    return this.http.get(`${this.baseUrl}/person/${id}/external_ids`, {
      params: new HttpParams()
        .set("api_key", this.API_KEY)
        .set("language", this.lang)
    });
  }

  getBestMovies(id: number) {
    return this.http.get(`${this.baseUrl}/discover/movie`, {
      params: new HttpParams()
      .set('api_key', this.API_KEY)
      .set('language', this.lang)
      .set('with_people', `${id}`)
      .set('sort_by', 'popularity.desc')
    })
  }

  currentDate: string = new Date().toDateString()

  getTheatreMovies() {
    return this.http.get(`${this.baseUrl}/discover/movie`, {
      params: new HttpParams()
      .set('api_key', this.API_KEY)
      .set('language', this.lang)
      .set('primary_release_date.lte', this.currentDate)
    })
  }
}
