import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  createNewUser(email, password) {
    return new Promise (
      (resolve, reject) => {
        firebase.auth().createUserWithEmailAndPassword(email, password).then(
          () => { resolve();},
          (error) => { reject(error)}
        )
      }
    )
  }

  signInUser(email, password) {
    return new Promise (
      (resolve, reject) => {
        firebase.auth().signInWithEmailAndPassword(email, password).then(
          () => { resolve(); },
          (error) => { reject(error); }
        )
      }
    )
  }

  signOutUser() {
    firebase.auth().signOut();
  }
}
